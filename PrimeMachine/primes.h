/* Primzahlentest mit Division, Zahlenbereich Integer
   (c)kova, 19.10.2021
*/
#include <math.h> 
// Die Funktion gibt true zurueck, wenn primeCandidate prim ist
bool isPrime(int primeCandidate) {
  if(primeCandidate == 1) return false;     // 1 ist nicht prim
  if(primeCandidate == 2) return true;      // 2 ist prim
  if(primeCandidate % 2 == 0) return false; // gerade Zahlen weg 
  // Dividieren bis zur Wurzel von primeCandidate
  for (int div=3;div<=sqrt((double)primeCandidate);div+=2){
    if (primeCandidate % div == 0) return false; // teilbar weg
  }
  return true;  // war nicht teilbar, ist prim
}
